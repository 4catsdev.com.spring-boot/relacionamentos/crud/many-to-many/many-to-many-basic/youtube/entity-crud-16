package com.fourcatsdev.entitycrud.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fourcatsdev.entitycrud.modelo.Habilidade;

public interface HabilidadeRepositorio extends JpaRepository<Habilidade, Long> {

}
